#include "destinationpage.h"
#include "ui_destinationpage.h"

DestinationPage::DestinationPage(QWidget* parent, QString* destinationFolder) :
	QWizardPage(parent),
	ui(new Ui::DestinationPage)
{
	this->destinationFolder = destinationFolder;
	ui->setupUi(this);
	this->setLayout(ui->verticalLayout);

	// Prefill existing folder path with Pictures folder
	parentFolder = QStandardPaths::standardLocations(QStandardPaths::PicturesLocation).first();
	ui->existingFolderLabel->setText(parentFolder);
	updateNewFolder();

	// Update the UI when the new folder name changes
	connect(ui->newFolderEntry_5, SIGNAL(textChanged(const QString &)), this, SLOT(updateNewFolder()));

	// Activate browse for parent directory button
	connect(ui->existingFolderButton, SIGNAL(clicked()), this, SLOT(browseForParentDir()));
}

DestinationPage::~DestinationPage()
{
	delete ui;
}

void DestinationPage::browseForParentDir()
{
	QString selected = QFileDialog::getExistingDirectory(nullptr, QString(), parentFolder);

	if (!selected.isEmpty())
	{
		parentFolder = selected;
		ui->existingFolderLabel->setText(parentFolder);
		updateDestinationFolder();
	}
}

void DestinationPage::updateNewFolder()
{
	newFolder = ui->newFolderEntry_5->text();
	updateDestinationFolder();
}

void DestinationPage::updateDestinationFolder()
{
	*destinationFolder = QDir::cleanPath(parentFolder + QDir::separator() + newFolder);
	ui->destinationLabel->setText(*destinationFolder);
}
