#ifndef SOURCEPAGE_H
#define SOURCEPAGE_H

#include <QDir>
#include <QDirIterator>
#include <QList>
#include <QMessageBox>
#include <QProgressDialog>
#include <QStorageInfo>
#include <QWizardPage>

#include "multidrivedialog.h"

namespace Ui {
	class SourcePage;
}

class SourcePage : public QWizardPage
{
	Q_OBJECT

public:
	explicit SourcePage(QWidget* = nullptr, QStringList* = nullptr);
	~SourcePage();
	void resizeEvent(QResizeEvent*);
	void initializePage();
	void showEvent(QShowEvent*);

private:
	static const int iconSizeDelta = 50;
	Ui::SourcePage* ui;
	QStringList* selectedMedia;
	void updateWizard();
	void showThumbs(QStringList, QProgressDialog*);
	void resizeThumbs(int = 0);
	QString getDrivePath();
	QStringList scanDrive(QString);

private slots:
	void increaseThumbSize();
	void decreaseThumbSize();
	void selectionChanged();
};

/**
 * @brief findMedia recursively locates images and videos.
 * @return A list of applicable QFile objects found in the directory.
 */
QStringList findMedia(QDir);

class FSIO {
public:
	static const QStringList mediaExtensions;
};

#endif // SOURCEPAGE_H
