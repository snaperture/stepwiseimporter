#ifndef MULTIDRIVEDIALOG_H
#define MULTIDRIVEDIALOG_H

#include <QDialog>
#include <QStorageInfo>

namespace Ui {
	class MultiDriveDialog;
}

class MultiDriveDialog : public QDialog
{
	Q_OBJECT

public:
	explicit MultiDriveDialog(QWidget* parent = nullptr,
							  QList<QStorageInfo>* = nullptr,
							  QString* = nullptr);
	~MultiDriveDialog();

private:
	Ui::MultiDriveDialog* ui;
	QList<QStorageInfo>* drives;
	QString* selectedRoot;

private slots:
	void updateSelectedDrive();
};

#endif // MULTIDRIVEDIALOG_H
