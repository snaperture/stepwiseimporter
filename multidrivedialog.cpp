#include "multidrivedialog.h"
#include "ui_multidrivedialog.h"

MultiDriveDialog::MultiDriveDialog(QWidget* parent,
								   QList<QStorageInfo>* drives,
								   QString* selectedRoot) :
	QDialog(parent),
	ui(new Ui::MultiDriveDialog)
{
	this->selectedRoot = selectedRoot;

	setFixedSize(400, 200);
	ui->setupUi(this);

	connect(ui->driveSelect, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(updateSelectedDrive()));

	foreach (QStorageInfo drive, *drives) {
		ui->driveSelect->addItem(drive.displayName(), drive.rootPath());
	}
}

MultiDriveDialog::~MultiDriveDialog()
{
	delete ui;
}

void MultiDriveDialog::updateSelectedDrive()
{
	*selectedRoot = ui->driveSelect->currentData().toString();
}
