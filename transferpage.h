#ifndef TRANSFERPAGE_H
#define TRANSFERPAGE_H

#include <QAbstractButton>
#include <QDate>
#include <QDir>
#include <QFile>
#include <QMessageBox>
#include <QProgressDialog>
#include <QThread>
#include <QWizardPage>

namespace Ui {
	class TransferPage;
}

class TransferPage : public QWizardPage
{
	Q_OBJECT

public:
	explicit TransferPage(QWidget* parent = nullptr, QStringList* = nullptr, QString* = nullptr);
	~TransferPage();
	void initializePage();

private:
	Ui::TransferPage* ui;
	QStringList uncopiedSources;
	QStringList* sources;
	QString* destination;
	void copyFiles();
};

#endif // TRANSFERPAGE_H
