#ifndef DESTINATIONPAGE_H
#define DESTINATIONPAGE_H

#include <QDateTime>
#include <QDir>
#include <QFileDialog>
#include <QStandardPaths>
#include <QWizardPage>

namespace Ui {
	class DestinationPage;
}

class DestinationPage : public QWizardPage
{
	Q_OBJECT

public:
	explicit DestinationPage(QWidget* = nullptr, QString* = nullptr);
	~DestinationPage();

private:
	Ui::DestinationPage* ui;
	QString parentFolder;
	QString newFolder;
	QString* destinationFolder;
	void updateDestinationFolder();

private slots:
	void browseForParentDir();
	void updateNewFolder();
};

#endif // DESTINATIONPAGE_H
